package me.nitram.joinplugin;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.permissions.PermissionAttachment;

public class JoinListener implements Listener
{
    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event)
    {
        Player player = event.getPlayer();
        event.setJoinMessage(ChatColor.GREEN + "+ " + ChatColor.GRAY + player.getName());

        PermissionAttachment attachment = player.addAttachment(JoinPlugin.getInstance());
        for (String permission : JoinPlugin.getInstance()
            .getConfig()
            .getStringList("Permissions")
        ) {
            attachment.setPermission(permission, true);
        }
    }

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent event) {
        Player player = event.getPlayer();
        event.setQuitMessage(ChatColor.RED + "- " + ChatColor.GRAY + player.getName());
    }
}
