package me.nitram.joinplugin;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class HealCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender commandSender, Command cmd, String s, String[] args)
    {
        if ( ! (commandSender instanceof Player player)) {
            if (args.length > 0) {
                heal(args[0]);
            }

            return true;
        }

        if (args.length > 0) {
            if ( ! player.hasPermission("devschool.heal.others")) {
                player.sendMessage(ChatColor.RED + "You don't have permission to heal others!");
                return true;
            }

            if (heal(args[0])) {
                return true;
            }
        }

        if( ! player.hasPermission("devschool.heal")) {
            player.sendMessage(ChatColor.RED + "You don't have permission to heal!");
            return true;
        }

        player.setHealth(20);
        return true;
    }

    private boolean heal(String target) {
        Player targetPlayer = Bukkit.getPlayer(target);

        if (targetPlayer == null) {
            return false;
        }

        targetPlayer.setHealth(20);
        return true;
    }
}
