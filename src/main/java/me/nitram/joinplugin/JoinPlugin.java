package me.nitram.joinplugin;

import org.bukkit.plugin.java.JavaPlugin;
import java.util.Objects;

public class JoinPlugin extends JavaPlugin {

    private static JoinPlugin instance;


    public static JoinPlugin getInstance() {
        return instance;
    }


    @Override
    public void onEnable()
    {
        instance = this;
        System.out.println("Server zapnut");

        this.getServer().getPluginManager().registerEvents(new JoinListener(), this);
        Objects.requireNonNull(this.getCommand("heal")).setExecutor(new HealCommand());
    }


    @Override
    public void onDisable()
    {
        System.out.println("Server vypnut");
    }
}
